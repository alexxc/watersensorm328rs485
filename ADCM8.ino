#include <stdint.h>
#include <avr/wdt.h>

uint8_t mcusr_mirror __attribute__((section(".noinit")));
void get_mcusr(void) __attribute__((naked))  __attribute__((used)) __attribute__((section(".init3")));

void get_mcusr(void) {
	mcusr_mirror = MCUSR;
	MCUSR = 0;
	wdt_disable();
}
#define DEBUG 1

#include <Arduino.h>
#include <SimpleModbusSlave.h>
#ifdef DEBUG
#include "SoftwareSerial.h"
#endif // DEBUG


#ifdef DEBUG
#include "SoftwareSerial.h"
#endif // DEBUG


enum {
	STATE,
	ADC_DATA,
	TOTAL_REGS_SIZE
};


enum STATE_T { STATE_IDLE, STATE_SET_PLUS, STATE_START_MEAS, STATE_MEAS, STATE_PROCESS };

#ifdef DEBUG
char str[32];
SoftwareSerial Debug(3, 4);
#endif // DEBUG



volatile uint8_t state;
volatile uint32_t state_time;
// ����� ������ ��� ������ �������
uint16_t adc[4];
uint8_t slave_id;
uint16_t adc_buf[4];
uint8_t adc2read = 0;

#define DE_PIN		2
#define UART_SPEED	19200
//#define UART_SPEED	2400

#define ClearBit(reg, bit)       reg &= (~(1<<(bit)))
#define SetBit(reg, bit)          reg |= (1<<(bit))
#define BitIsClear(reg, bit)    ((reg & (1<<(bit))) == 0)
#define BitIsSet(reg, bit)       ((reg & (1<<(bit))) != 0)

// ADDR 
#define ADDR0 PC5
#define ADDR1 PC4
#define ADDR2 PC3
#define ADDR3 PC2

#define SETUP_TIME_MS	100
#define MEAS_TIME_MS	10
#define ADC_PIN_IN		A0
#define ADC_PIN_OUT		A1

uint16_t holdingRegs[TOTAL_REGS_SIZE]; 


uint8_t get_slave_id() {
	uint8_t t, t1;
	DDRC &=~0b00111100;				// ����
	PORTC |= (0b00111100);			// � ���������� � +
	t1 = 0;
	t = PINC;
	if (BitIsClear(t, ADDR0))
		t1 |= (1<<0);
	if (BitIsClear(t, ADDR1))
		t1 |= (1<<1);
	if (BitIsClear(t, ADDR2))
		t1 |= (1<<2);
	if (BitIsClear(t, ADDR3))
		t1 |= (1<<3);
	t1++;
	return t1;
}

ISR(ADC_vect) {
	uint16_t raw;
	raw = ADCL & 0b11111111;
	raw += (ADCH<<8);							// shift from low level to high level ADC, from 8bit to 10bit
	adc[adc2read++]=raw;
}


void adc_init() {
	DDRC &= ~(1<<PC0);	// ����
	PORTC &= ~(1<<PC0);	// ��� ��������
	DDRC |= (1<<PC1);	// �����
	ADMUX = (0<<REFS1)|(1<<REFS0);											// �����
	ADCSRA |=(1<<ADEN)| (1<<ADIE) |(1<<ADPS2)|(1<<ADPS1)|(0<<ADPS0);		// �������� ���, ����������� ������������ (div64)
}


void adc_read_start() {
//	uint16_t result = 0;
	ADCSRA |= (1 << ADSC);				//start ADC conversation
//	while (ADCSRA & (1 << ADSC));		//wait for conversation complete
//	result = ADCH;						//get the result of conversation
//	return result;						//return the result
}

void init_modbus() {
	slave_id = get_slave_id();
#ifdef DEBUG
	sprintf(str, "MODBUS init, slave ID %d\r\n", slave_id);
	Debug.print(str);
#endif // DEBUG
	modbus_configure(UART_SPEED, slave_id, DE_PIN, TOTAL_REGS_SIZE, 1);
}

void setup() {
	delay(100);
	adc_init();
#ifdef DEBUG
	Debug.begin(UART_SPEED);
	_delay_ms(500);
#endif // DEBUG
	init_modbus();
	state_time = millis();
	//	����� ��������� �������� �����
	state=STATE_SET_PLUS;
	wdt_enable(WDTO_4S);
}


void loop() {
	wdt_reset();
	modbus_update(holdingRegs);
	switch (state) {
	case STATE_IDLE:
		// ������ > ������ � ���������� ���������
		if ((millis()-state_time)>100) {
			state_time = millis();
			state = STATE_SET_PLUS;
		}
		break;
	case STATE_SET_PLUS:
		PORTC |= (1<<1);
		state_time = millis();
		state = STATE_START_MEAS;
		break;
	case STATE_START_MEAS:
		// ��������� 10ms �� ��������� +
		if ((millis()-state_time)>SETUP_TIME_MS) {
			adc2read = 0;
			state_time = millis();
			state = STATE_MEAS;
		}
		break;
	case STATE_MEAS:
		if (millis()-state_time>MEAS_TIME_MS) {
			adc_read_start();
			if (adc2read==4) {
				PORTC &= ~(1<<1);
				state = STATE_PROCESS;
				adc2read = 0;
			}
			state_time = millis();
		}
		break;
	case  STATE_PROCESS:
		holdingRegs[ADC_DATA] = 0;
		for (uint8_t i =0; i<4; i++) {
			holdingRegs[ADC_DATA] += adc[i];
		}
		holdingRegs[ADC_DATA] = holdingRegs[ADC_DATA] / 4;
		holdingRegs[STATE] = holdingRegs[ADC_DATA]>200 ? 0x00FF : 0;
		state_time = millis();
		uint8_t t = get_slave_id();
		if (t!=slave_id) {
			init_modbus();
		}
#ifdef DEBUG
		sprintf(str, "ID: %d, ADC: %d, state: %d\r\n", slave_id, holdingRegs[ADC_DATA], holdingRegs[STATE]);
		Debug.print(str);
#endif // DEBUG

		state = STATE_IDLE;
		break;
	default:
		break;
	}
}
