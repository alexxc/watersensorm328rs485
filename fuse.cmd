@echo off
rem
rem read fuse
rem avrdude -C avrdude.conf -c usbasp -B 115200 -p m328p -U signature:r:sign.tmp:r -U lock:r:lock.tmp:r -U calibration:r:calib.tmp:r -U hfuse:r:hfuse.tmp:r -U lfuse:r:lfuse.tmp:r -U efuse:r:efuse.tmp:r -v
rem H=DF L=FF E=FD

rem write fuse
rem MEGA328 - вннещний кварц
rem avrdude -C avrdude.conf -c usbasp -B 115200 -p m328p -U hfuse:w:0xDF:m -U lfuse:w:0xFF:m -U lock:w:0x3F:m -U efuse:w:0xFD:m -v

rem MEGA328 - внутренний кварц, 8мГц
rem avrdude -C avrdude.conf -c usbasp -B 115200 -p m328p -U hfuse:w:0xDF:m -U lfuse:w:0xE2:m -U lock:w:0x3F:m -U efuse:w:0xFD:m -v

d:\YandexDisk\ALEX\WORK\ELECTRONIX\UTIL\SinaProg\USBASP\avrdude.exe -C "d:\YandexDisk\ALEX\WORK\ELECTRONIX\UTIL\SinaProg\USBASP\avrdude.conf" -c usbasp -B 115200 -p m328p -U hfuse:w:0xD9:m -U lfuse:w:0xE2:m -U efuse:w:0xFD:m 

rem Write pgm
rem d:\YandexDisk\ALEX\WORK\ELECTRONIX\UTIL\SinaProg\M328\avrdude.exe -C "d:\YandexDisk\ALEX\WORK\ELECTRONIX\UTIL\SinaProg\M328\avrdude.conf" -c usbasp -B 115200 -p m328p -U flash:w:"Release\ADCM8.hex":a -v
